This image runs a WireGuard connection and exposes it via a Socks5 proxy.

You can either mount a wireguard config file, a tar.gz archive containing multiple config files or a directory containing multiple config files to `/wg.conf`.
The container will automatically detect what to do.

Env: `RECONNECT_TIME` - Reconnection time in minutes after WG will close the connection and reconnect, `0` to disable. Supply a range (e.g. `5-10`) to select randomly each time. Defaults to `0`.

The container needs to be started with `--privileged --cap-add NET_ADMIN`!

### run with plain docker
`/absolute/path/to/my/wg.conf` can be a wireguard config file or a directory/.tar.gz containing such files.
```
docker run --rm -d --privileged --cap-add NET_ADMIN \
    -v /absolute/path/to/my/wg.conf:/wg.conf:ro,Z \
    -p 127.10.80.1:1080:1080 \
    fluse1367/wireguard-socks
```

### run with docker compose
```yml
services:
    wireguard-socks:
        image: fluse1367/wireguard-socks
        restart: unless-stopped
        cap_add: ['NET_ADMIN']
        privileged: true
        environment: ['RECONNECT_TIME=60']
        volumes: ['./wg_confs.tar.gz:/wg.conf:ro,Z']
        ports: ['127.10.80.1:1080:1080'] 
```